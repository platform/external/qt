/****************************************************************************
** Meta object code from reading C++ file 'containerwidget_taskmenu.h'
**
** Created: Mon Jul 16 20:02:02 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../taskmenu/containerwidget_taskmenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'containerwidget_taskmenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_qdesigner_internal__ContainerWidgetTaskMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      45,   44,   44,   44, 0x08,
      65,   44,   44,   44, 0x08,
      75,   44,   44,   44, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenu[] = {
    "qdesigner_internal::ContainerWidgetTaskMenu\0"
    "\0removeCurrentPage()\0addPage()\0"
    "addPageAfter()\0"
};

const QMetaObject qdesigner_internal::ContainerWidgetTaskMenu::staticMetaObject = {
    { &QDesignerTaskMenu::staticMetaObject, qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenu,
      qt_meta_data_qdesigner_internal__ContainerWidgetTaskMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &qdesigner_internal::ContainerWidgetTaskMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *qdesigner_internal::ContainerWidgetTaskMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *qdesigner_internal::ContainerWidgetTaskMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenu))
        return static_cast<void*>(const_cast< ContainerWidgetTaskMenu*>(this));
    return QDesignerTaskMenu::qt_metacast(_clname);
}

int qdesigner_internal::ContainerWidgetTaskMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDesignerTaskMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: removeCurrentPage(); break;
        case 1: addPage(); break;
        case 2: addPageAfter(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_qdesigner_internal__WizardContainerWidgetTaskMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_qdesigner_internal__WizardContainerWidgetTaskMenu[] = {
    "qdesigner_internal::WizardContainerWidgetTaskMenu\0"
};

const QMetaObject qdesigner_internal::WizardContainerWidgetTaskMenu::staticMetaObject = {
    { &ContainerWidgetTaskMenu::staticMetaObject, qt_meta_stringdata_qdesigner_internal__WizardContainerWidgetTaskMenu,
      qt_meta_data_qdesigner_internal__WizardContainerWidgetTaskMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &qdesigner_internal::WizardContainerWidgetTaskMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *qdesigner_internal::WizardContainerWidgetTaskMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *qdesigner_internal::WizardContainerWidgetTaskMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__WizardContainerWidgetTaskMenu))
        return static_cast<void*>(const_cast< WizardContainerWidgetTaskMenu*>(this));
    return ContainerWidgetTaskMenu::qt_metacast(_clname);
}

int qdesigner_internal::WizardContainerWidgetTaskMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContainerWidgetTaskMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_qdesigner_internal__MdiContainerWidgetTaskMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_qdesigner_internal__MdiContainerWidgetTaskMenu[] = {
    "qdesigner_internal::MdiContainerWidgetTaskMenu\0"
};

const QMetaObject qdesigner_internal::MdiContainerWidgetTaskMenu::staticMetaObject = {
    { &ContainerWidgetTaskMenu::staticMetaObject, qt_meta_stringdata_qdesigner_internal__MdiContainerWidgetTaskMenu,
      qt_meta_data_qdesigner_internal__MdiContainerWidgetTaskMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &qdesigner_internal::MdiContainerWidgetTaskMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *qdesigner_internal::MdiContainerWidgetTaskMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *qdesigner_internal::MdiContainerWidgetTaskMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__MdiContainerWidgetTaskMenu))
        return static_cast<void*>(const_cast< MdiContainerWidgetTaskMenu*>(this));
    return ContainerWidgetTaskMenu::qt_metacast(_clname);
}

int qdesigner_internal::MdiContainerWidgetTaskMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContainerWidgetTaskMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_qdesigner_internal__ContainerWidgetTaskMenuFactory[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenuFactory[] = {
    "qdesigner_internal::ContainerWidgetTaskMenuFactory\0"
};

const QMetaObject qdesigner_internal::ContainerWidgetTaskMenuFactory::staticMetaObject = {
    { &QExtensionFactory::staticMetaObject, qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenuFactory,
      qt_meta_data_qdesigner_internal__ContainerWidgetTaskMenuFactory, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &qdesigner_internal::ContainerWidgetTaskMenuFactory::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *qdesigner_internal::ContainerWidgetTaskMenuFactory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *qdesigner_internal::ContainerWidgetTaskMenuFactory::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__ContainerWidgetTaskMenuFactory))
        return static_cast<void*>(const_cast< ContainerWidgetTaskMenuFactory*>(this));
    return QExtensionFactory::qt_metacast(_clname);
}

int qdesigner_internal::ContainerWidgetTaskMenuFactory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QExtensionFactory::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
